package yi.master.coretest.message.process.config;

/**
 * 河南豫事办报文处理器参数
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/6/5 9:41
 */
public class HeNanYsbGjjMsgProcessParameter {
    /**
     * 私钥，每个地市不一样
     */
    private String priKey;
    /**
     * 公钥，每个地市不一样
     */
    private String pubKey;

    /**
     * 某些地市的特殊请求头生成需要的参数
     */
    private String clientId;
    /**
     * 某些地市的特殊请求头生成需要的参数
     */
    private String secretKey;


    public void setPriKey(String priKey) {
        this.priKey = priKey;
    }

    public void setPubKey(String pubKey) {
        this.pubKey = pubKey;
    }

    public String getPriKey() {
        return priKey;
    }

    public String getPubKey() {
        return pubKey;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getClientId() {
        return clientId;
    }

    public String getSecretKey() {
        return secretKey;
    }
}
